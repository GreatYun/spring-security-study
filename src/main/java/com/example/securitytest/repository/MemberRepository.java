package com.example.securitytest.repository;

import com.example.securitytest.domain.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member , Long> {

    public Member findByUserId(String userId);
}
