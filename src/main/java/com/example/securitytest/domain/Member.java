package com.example.securitytest.domain;

import com.example.securitytest.enumuration.ROLE;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Table(name = "tb_member")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
public class Member  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pkid")
    private Long pkid;

    @Column(name = "user_id")
    @NotNull
    private String userId;

    @Column(name = "user_pwd")
    @NotNull
    private String userPwd;

    @Column(name = "role" , length = 50)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private ROLE role;

    @Column(name = "reg_date" , updatable = false)
    @NotNull
    private LocalDateTime regDate;

}
