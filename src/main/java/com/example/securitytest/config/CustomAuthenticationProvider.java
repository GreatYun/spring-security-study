package com.example.securitytest.config;

import com.example.securitytest.service.SecurityLoginService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class CustomAuthenticationProvider implements AuthenticationProvider {


    @Autowired
    private SecurityLoginService securityLoginService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        UsernamePasswordAuthenticationToken authToken = (UsernamePasswordAuthenticationToken) authentication;

        UserDetails userDetails = securityLoginService.loadUserByUsername(authToken.getName());
        if(userDetails == null) {
            throw new UsernameNotFoundException("ID가 잘못되었습니다.");
        }

        if(!passwordEncoder.matches(authentication.getCredentials().toString() , userDetails.getPassword())) {
            log.info("Password Not Matching !! ");
            throw new BadCredentialsException("not matching");
        }

        log.info("userDetails role : " + userDetails.getAuthorities().toString());
//        log.info("userDetails role : " + userDetails.getAuthorities().toString());

        return new UsernamePasswordAuthenticationToken(userDetails , null , userDetails.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
