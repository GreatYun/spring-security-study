package com.example.securitytest.enumuration;

public enum ROLE  implements EnumModel{
    MANAGER("매니저") , RESTAPI("API") , SUPER("최고관리자") , PARTNER("파트너관리자") , USER("일반유저");

    private String description;

    private ROLE (String description) {
        this.description = description;
    }
    public String getDescription() {
        return description;
    }

    @Override
    public String getKey() {
        return name();
    }

    @Override
    public String getValue() {
        return description;
    }
}
