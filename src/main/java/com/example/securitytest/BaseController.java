package com.example.securitytest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
@Slf4j
public class BaseController {

    @GetMapping("/admin")
    public String home() {
        return "home";
    }

    @GetMapping("/admin/main")
    public String adminmain() {
        return "admin/main";
    }


    @GetMapping("/admin/login")
    public String adminlogin() {
        log.info("admin/login");
        return "admin/login";
    }
    @GetMapping("/login/admin")
    public String loginadmin() {
        log.info("/login/admin");
        return "admin/login";
    }

    @GetMapping("/login")
    public ModelAndView login(Map<String , Object> model, @RequestParam(value = "error" , required = false) String error) {
        log.info(error == null ? "null" : error);
        if(error == null) {
            model.put("error" , "false");
        } else {
            model.put("error" , "true");
        }
        return new ModelAndView("admin/login/adminLogin" , model);
    }



    @GetMapping("/user")
    public String user() {
        return "user";
    }

    @GetMapping("/user/main")
    public String usermain() {
        return "user/main";
    }

    @GetMapping("/user/login")
    public String userlogin() {
        return "user/login";
    }



}
