package com.example.securitytest.enumuration;

public interface EnumModel {
    public String getKey();
    public String getValue();
}
