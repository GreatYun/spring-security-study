package com.example.securitytest;

import com.example.securitytest.domain.Member;
import com.example.securitytest.enumuration.ROLE;
import com.example.securitytest.repository.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class AppRunner  implements CommandLineRunner {

    @Autowired
    private MemberRepository memberRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void run(String... args) throws Exception {
        Member member = Member.builder()
        .regDate(LocalDateTime.now())
                .role(ROLE.SUPER)
                .userId("admin")
                .userPwd(passwordEncoder.encode("1234"))
        .build();

        memberRepository.save(member);

        member = Member.builder()
                .role(ROLE.USER)
                .userId("user")
                .userPwd(passwordEncoder.encode("1234"))
                .regDate(LocalDateTime.now())
                .build();

        memberRepository.save(member);

    }
}
