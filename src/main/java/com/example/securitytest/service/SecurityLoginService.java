package com.example.securitytest.service;

import com.example.securitytest.config.SecurityUser;
import com.example.securitytest.domain.Member;
import com.example.securitytest.repository.MemberRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class SecurityLoginService implements UserDetailsService {

    @Autowired
    private MemberRepository memberRepository;

    @Override
    public UserDetails loadUserByUsername(String loginId) throws UsernameNotFoundException {
        User user = null;
        Member member = null;

        log.info("admin id : " + loginId);

        if(!loginId.equalsIgnoreCase("admin")) {
            return null;
        }

        member = memberRepository.findByUserId(loginId);
        if(member == null) {
            throw new UsernameNotFoundException("ID가 존재하지 않습니다.");
        } else {
            user = new SecurityUser(member);
        }

        return user;
    }

}
