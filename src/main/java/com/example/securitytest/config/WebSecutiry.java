package com.example.securitytest.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;



@EnableWebSecurity
@Configuration
public class WebSecutiry  {

    @Configuration
    @Order(1)
    public static class ApiWebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {
        @Autowired
        private  CustomAuthenticationProvider customAuthenticationProvider;

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.csrf().disable();
            http.authorizeRequests().antMatchers("/static/** , /js/** , /css/**").permitAll();
            http.antMatcher("/admin/**")
                    .authorizeRequests()
                    .anyRequest().hasAnyRole("ADMIN,MANAGER,PARTNER,SUPER")
                    //.and().formLogin().loginPage("/login/admin")
                    .and().formLogin()
                    .defaultSuccessUrl("/admin/main" , true)
                    .failureForwardUrl("/login/admin?error")
                    .and()
                    .logout().logoutUrl("/logout")
                    .logoutSuccessUrl("/login/admin")
                    .invalidateHttpSession(true)
                    .deleteCookies("JSESSIONID")
                    .and()
                    .authenticationProvider(customAuthenticationProvider)
                    ;
            /*
            http.authorizeRequests().antMatchers("/admin/**")
                    .hasAnyRole("ADMIN,MANAGER,PARTNER,SUPER")
                    .and()
                    .formLogin().loginPage("/admin/login")
                    .defaultSuccessUrl("/admin/main" , true)
                    .failureForwardUrl("/login?error")
                    .and()
                    .logout().logoutUrl("/logout")
                    .logoutSuccessUrl("/admin/login")
                    .invalidateHttpSession(true)
                    .deleteCookies("JSESSIONID")
                    .and()
                    .authenticationProvider(customAuthenticationProvider)
            ;

             */
        }
    }

    @Configuration
    public static class WebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {
        @Autowired
        private  CustomAuthenticationProvider customAuthenticationProvider;

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.antMatcher("/user/**")
                    .authorizeRequests()
                    .anyRequest().hasAnyRole("USER")
                    .and().formLogin().loginPage("/user/login")
            ;
            /*
            http.authorizeRequests().antMatchers("/user/**")
                    .hasAnyRole("USER")
                    .and()
                    .formLogin().loginPage("/user/login")
                    .defaultSuccessUrl("/user/main" , true)
                    .failureForwardUrl("/login?error")
                    .and()
                    .logout().logoutUrl("/logout")
                    .logoutSuccessUrl("/user/login")
                    .invalidateHttpSession(true)
                    .deleteCookies("JSESSIONID")
                    .and()
                    .authenticationProvider(customAuthenticationProvider)
            ;

             */
        }

    }



    /*
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable();
        http.authorizeRequests().antMatchers("/static/** , /js/** , /css/**").permitAll();
        http.authorizeRequests().antMatchers("/admin/**")
                .hasAnyRole("ADMIN,MANAGER,PARTNER,SUPER")
                .and()
                    .formLogin().loginPage("/admin/login")
                    .defaultSuccessUrl("/admin/main" , true)
                    .failureForwardUrl("/login?error")
                .and()
                    .logout().logoutUrl("/logout")
                    .logoutSuccessUrl("/admin/login")
                    .invalidateHttpSession(true)
                    .deleteCookies("JSESSIONID")
                .and()
                    .authenticationProvider(customAuthenticationProvider)
        ;

        http.authorizeRequests().antMatchers("/user/**")
                .hasAnyRole("USER")
                .and()
                .formLogin().loginPage("/user/login")
                .defaultSuccessUrl("/user/main" , true)
                .failureForwardUrl("/login?error")
                .and()
                .logout().logoutUrl("/logout")
                .logoutSuccessUrl("/user/login")
                .invalidateHttpSession(true)
                .deleteCookies("JSESSIONID")
            .and()
                .authenticationProvider(customAuthenticationProvider)
        ;

    }

     */


}
