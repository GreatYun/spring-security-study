package com.example.securitytest.config;

import com.example.securitytest.domain.Member;
import lombok.Getter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.ArrayList;
import java.util.List;

@Getter
public class SecurityUser extends User {
    private static final String ROLE_PREFIX = "ROLE_";
    public SecurityUser (Member member) {
//        super(admin.getAdminId() , admin.getAdminPwd() , Arrays.asList(new SimpleGrantedAuthority(getRole(admin.getRole().getKey()))));
        super(member.getUserId() , member.getUserPwd() , getRoles(member));
    }

    private static String getRole(String key) {
        return ROLE_PREFIX + key;
    }

    private static List<SimpleGrantedAuthority> getRoles(Member member) {
        ArrayList<SimpleGrantedAuthority> authorities = new ArrayList<>();
        if(member.getUserId().equals("apiuser")) {
            authorities.add(new SimpleGrantedAuthority("ROLE_API"));
        } else if(member.getUserId().equals("greatyun")) {
            authorities.add(new SimpleGrantedAuthority("ROLE_SUPER"));
        } else {
            authorities.add(new SimpleGrantedAuthority(ROLE_PREFIX + member.getRole().getKey()));
        }
        return authorities;
    }

}
